# Makefile for latex slides

TARGET_SOURCES=$(wildcard **/*.tex)
TARGET=$(subst .tex,,$(TARGET_SOURCES))
TARGET_DIRECTORIES=$(shell find . -maxdepth 1 -type d -name '^[0-9]*')
TARGET_PDFS=$(subst .tex,.pdf,$(TARGET_SOURCES))

.PHONY: all
all: $(TARGET)

.PHONY: clean
clean:
	$(foreach D,$(TARGET_DIRECTORIES),make -C $(D) clean)

%:
	make -C $(dir $@) all
